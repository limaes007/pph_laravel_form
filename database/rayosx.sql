-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-05-2019 a las 05:55:11
-- Versión del servidor: 10.1.40-MariaDB
-- Versión de PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tramiteslinea`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rayosx_direccion`
--

CREATE TABLE `rayosx_direccion` (
  `id_direccion_rayosx` int(11) NOT NULL,
  `id_tramite_rayosx` int(11) NOT NULL,
  `dire_entidad` varchar(250) NOT NULL,
  `sede_entidad` varchar(250) NOT NULL,
  `email_entidad` varchar(250) NOT NULL,
  `depto_entidad` int(11) NOT NULL,
  `mpio_entidad` int(11) NOT NULL,
  `celular_entidad` varchar(20) NOT NULL,
  `indicativo_entidad` varchar(5) NOT NULL,
  `telefono_entidad` varchar(15) NOT NULL,
  `extension_entidad` varchar(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Tabla direccion entidad Rayos X';

--
-- Volcado de datos para la tabla `rayosx_direccion`
--

INSERT INTO `rayosx_direccion` (`id_direccion_rayosx`, `id_tramite_rayosx`, `dire_entidad`, `sede_entidad`, `email_entidad`, `depto_entidad`, `mpio_entidad`, `celular_entidad`, `indicativo_entidad`, `telefono_entidad`, `extension_entidad`) VALUES
(1, 1, 'Calle 3', 'SUR', 'asd@asd.com', 11, 11001, '3216548799', '1', '3648775', '1234');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rayosx_director`
--

CREATE TABLE `rayosx_director` (
  `id_director_rayosx` int(11) NOT NULL,
  `id_tramite_rayosx` int(11) NOT NULL,
  `talento_pnombre` varchar(150) NOT NULL,
  `talento_snombre` varchar(150) NOT NULL,
  `talento_papellido` varchar(150) NOT NULL,
  `talento_sapellido` varchar(150) NOT NULL,
  `talento_tdocumento` int(11) NOT NULL,
  `talento_ndocumento` varchar(20) NOT NULL,
  `talento_lexpedicion` varchar(150) NOT NULL,
  `talento_correo` varchar(150) NOT NULL,
  `talento_titulo` varchar(150) NOT NULL,
  `talento_universidad` varchar(150) NOT NULL,
  `talento_libro` varchar(150) NOT NULL,
  `talento_registro` varchar(150) NOT NULL,
  `talento_fecha_diploma` varchar(150) NOT NULL,
  `talento_resolucion` varchar(150) NOT NULL,
  `talento_fecha_convalida` varchar(150) NOT NULL,
  `talento_nivel` varchar(150) NOT NULL,
  `talento_titulo_pos` varchar(150) NOT NULL,
  `talento_universidad_pos` varchar(150) NOT NULL,
  `talento_libro_pos` varchar(150) NOT NULL,
  `talento_registro_pos` varchar(150) NOT NULL,
  `talento_fecha_diploma_pos` varchar(150) NOT NULL,
  `talento_resolucion_pos` varchar(150) NOT NULL,
  `talento_fecha_convalida_pos` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Tabla director tecnico rayos x';

--
-- Volcado de datos para la tabla `rayosx_director`
--

INSERT INTO `rayosx_director` (`id_director_rayosx`, `id_tramite_rayosx`, `talento_pnombre`, `talento_snombre`, `talento_papellido`, `talento_sapellido`, `talento_tdocumento`, `talento_ndocumento`, `talento_lexpedicion`, `talento_correo`, `talento_titulo`, `talento_universidad`, `talento_libro`, `talento_registro`, `talento_fecha_diploma`, `talento_resolucion`, `talento_fecha_convalida`, `talento_nivel`, `talento_titulo_pos`, `talento_universidad_pos`, `talento_libro_pos`, `talento_registro_pos`, `talento_fecha_diploma_pos`, `talento_resolucion_pos`, `talento_fecha_convalida_pos`) VALUES
(1, 1, 'Jorge', 'Jose', 'Perez', 'Martinez', 1, '25648774', 'Bogota', 'asd@asd.com', '1', '21', '2', '132', '2002-03-04', '12', '2013-05-012', '1', 'Posgrado', '23', '25', '124', '2013-10-24', '256', '2013-12-24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rayosx_documentos`
--

CREATE TABLE `rayosx_documentos` (
  `id_documentos_rayosx` int(11) NOT NULL,
  `id_tramite_rayosx` int(11) NOT NULL,
  `documento` varchar(60) NOT NULL,
  `id_archivo` int(11) NOT NULL,
  `estado` char(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Tabla documentos adjuntos';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rayosx_encargado`
--

CREATE TABLE `rayosx_encargado` (
  `id_encargado_rayosx` int(11) NOT NULL,
  `id_tramite_rayosx` int(11) NOT NULL,
  `encargado_pnombre` varchar(150) NOT NULL,
  `encargado_snombre` varchar(150) NOT NULL,
  `encargado_papellido` varchar(150) NOT NULL,
  `encargado_sapellido` varchar(150) NOT NULL,
  `encargado_tdocumento` int(11) NOT NULL,
  `encargado_ndocumento` varchar(20) NOT NULL,
  `encargado_lexpedicion` varchar(150) NOT NULL,
  `encargado_correo` varchar(150) NOT NULL,
  `encargado_profesion` int(11) NOT NULL,
  `encargado_nivel` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Tabla encargado de proteccion';

--
-- Volcado de datos para la tabla `rayosx_encargado`
--

INSERT INTO `rayosx_encargado` (`id_encargado_rayosx`, `id_tramite_rayosx`, `encargado_pnombre`, `encargado_snombre`, `encargado_papellido`, `encargado_sapellido`, `encargado_tdocumento`, `encargado_ndocumento`, `encargado_lexpedicion`, `encargado_correo`, `encargado_profesion`, `encargado_nivel`) VALUES
(1, 1, 'Javier', 'Andres', 'Morales', 'Rivera', 1, '1035487796', 'Bogota', 'qwe@qwe.com', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rayosx_equipos`
--

CREATE TABLE `rayosx_equipos` (
  `id_equipo_rayosx` int(11) NOT NULL,
  `id_tramite_rayosx` int(11) NOT NULL,
  `categoria` int(11) NOT NULL,
  `categoria1` int(11) DEFAULT NULL,
  `categoria1_1` int(11) DEFAULT NULL,
  `categoria1_2` int(11) DEFAULT NULL,
  `categoria2` int(11) DEFAULT NULL,
  `equipo_generador` int(11) NOT NULL,
  `tipo_visualizacion` int(11) NOT NULL,
  `marca_equipo` varchar(250) NOT NULL,
  `modelo_equipo` varchar(250) NOT NULL,
  `serie_equipo` varchar(250) NOT NULL,
  `marca_tubo_rx` varchar(250) NOT NULL,
  `modelo_tubo_rx` varchar(250) NOT NULL,
  `serie_tubo_rx` varchar(250) NOT NULL,
  `tension_tubo_rx` varchar(250) NOT NULL,
  `contiene_tubo_rx` varchar(250) NOT NULL,
  `energia_fotones` varchar(250) NOT NULL,
  `energia_electrones` varchar(250) NOT NULL,
  `carga_trabajo` varchar(250) NOT NULL,
  `ubicacion_equipo` varchar(250) NOT NULL,
  `numero_permiso` varchar(250) NOT NULL,
  `anio_fabricacion` varchar(250) NOT NULL,
  `anio_fabricacion_tubo` varchar(250) NOT NULL,
  `estado` char(2) NOT NULL,
  `fecha_registro` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Tabla equipos de rayos X';

--
-- Volcado de datos para la tabla `rayosx_equipos`
--

INSERT INTO `rayosx_equipos` (`id_equipo_rayosx`, `id_tramite_rayosx`, `categoria`, `categoria1`, `categoria1_1`, `categoria1_2`, `categoria2`, `equipo_generador`, `tipo_visualizacion`, `marca_equipo`, `modelo_equipo`, `serie_equipo`, `marca_tubo_rx`, `modelo_tubo_rx`, `serie_tubo_rx`, `tension_tubo_rx`, `contiene_tubo_rx`, `energia_fotones`, `energia_electrones`, `carga_trabajo`, `ubicacion_equipo`, `numero_permiso`, `anio_fabricacion`, `anio_fabricacion_tubo`, `estado`, `fecha_registro`) VALUES
(1, 1, 1, 1, 1, NULL, NULL, 7, 3, 'asd', 'asd1', 'asd2', 'asd3', 'asd4', 'asd5', 'asd6', 'asd7', 'asd8', 'asd9', 'asd10', 'asd11', 'asd12', 'asd13', 'asd14', 'AC', '2018-06-08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rayosx_objprueba`
--

CREATE TABLE `rayosx_objprueba` (
  `id_obj_rayosx` int(11) NOT NULL,
  `id_tramite_rayosx` int(11) NOT NULL,
  `obj_nombre` varchar(150) NOT NULL,
  `obj_marca` varchar(150) NOT NULL,
  `obj_modelo` varchar(150) NOT NULL,
  `obj_serie` varchar(150) NOT NULL,
  `obj_calibracion` varchar(150) NOT NULL,
  `obj_vigencia` int(11) NOT NULL,
  `obj_fecha` varchar(150) NOT NULL,
  `obj_manual` int(11) NOT NULL,
  `obj_uso` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rayosx_toe`
--

CREATE TABLE `rayosx_toe` (
  `id_toe_rayosx` int(11) NOT NULL,
  `id_tramite_rayosx` int(11) NOT NULL,
  `toe_pnombre` varchar(150) NOT NULL,
  `toe_snombre` varchar(150) NOT NULL,
  `toe_papellido` varchar(150) NOT NULL,
  `toe_sapellido` varchar(150) NOT NULL,
  `toe_tdocumento` int(11) NOT NULL,
  `toe_ndocumento` varchar(20) NOT NULL,
  `toe_lexpedicion` varchar(150) NOT NULL,
  `toe_profesion` int(11) NOT NULL,
  `toe_nivel` int(11) NOT NULL,
  `toe_ult_entrenamiento` varchar(150) NOT NULL,
  `toe_pro_entrenamiento` varchar(150) NOT NULL,
  `toe_registro` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Tabla TOE rayos x';

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `rayosx_direccion`
--
ALTER TABLE `rayosx_direccion`
  ADD PRIMARY KEY (`id_direccion_rayosx`);

--
-- Indices de la tabla `rayosx_director`
--
ALTER TABLE `rayosx_director`
  ADD PRIMARY KEY (`id_director_rayosx`);

--
-- Indices de la tabla `rayosx_documentos`
--
ALTER TABLE `rayosx_documentos`
  ADD PRIMARY KEY (`id_documentos_rayosx`);

--
-- Indices de la tabla `rayosx_encargado`
--
ALTER TABLE `rayosx_encargado`
  ADD PRIMARY KEY (`id_encargado_rayosx`);

--
-- Indices de la tabla `rayosx_equipos`
--
ALTER TABLE `rayosx_equipos`
  ADD PRIMARY KEY (`id_equipo_rayosx`);

--
-- Indices de la tabla `rayosx_objprueba`
--
ALTER TABLE `rayosx_objprueba`
  ADD PRIMARY KEY (`id_obj_rayosx`);

--
-- Indices de la tabla `rayosx_toe`
--
ALTER TABLE `rayosx_toe`
  ADD PRIMARY KEY (`id_toe_rayosx`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `rayosx_direccion`
--
ALTER TABLE `rayosx_direccion`
  MODIFY `id_direccion_rayosx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `rayosx_director`
--
ALTER TABLE `rayosx_director`
  MODIFY `id_director_rayosx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `rayosx_documentos`
--
ALTER TABLE `rayosx_documentos`
  MODIFY `id_documentos_rayosx` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `rayosx_encargado`
--
ALTER TABLE `rayosx_encargado`
  MODIFY `id_encargado_rayosx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `rayosx_equipos`
--
ALTER TABLE `rayosx_equipos`
  MODIFY `id_equipo_rayosx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `rayosx_objprueba`
--
ALTER TABLE `rayosx_objprueba`
  MODIFY `id_obj_rayosx` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `rayosx_toe`
--
ALTER TABLE `rayosx_toe`
  MODIFY `id_toe_rayosx` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
