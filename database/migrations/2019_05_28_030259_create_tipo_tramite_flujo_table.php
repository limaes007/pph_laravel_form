<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipoTramiteFlujoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('tipo_tramite_flujo')){
            Schema::create('tipo_tramite_flujo', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('tipo_tramite',100);
                $table->unsignedInteger('tipo_tramite_id');
                $table->string('flujo_actividad_inicial',100);
                $table->string('flujo_actividad_final',100);
                $table->set('flujo_accion_permitida', ['avanzar', 'devolver','volver_inicio','inicio','fin']);
                $table->unsignedBigInteger('user_responsable');            
                $table->boolean('tipo_estado');

                $table->foreign('tipo_tramite_id')
                ->references('id')
                ->on('tipo_tramite');
                

                $table->foreign('user_responsable')
                ->references('id')
                ->on('users');        
            });
        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_tramite_flujo');
    }
}
