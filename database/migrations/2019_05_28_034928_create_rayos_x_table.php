<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRayosXTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared( file_get_contents( "database/rayosx.sql" ) );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rayosx_direccion');
        Schema::dropIfExists('rayosx_director');
        Schema::dropIfExists('rayosx_documentos');
        Schema::dropIfExists('rayosx_encargado');
        Schema::dropIfExists('rayosx_equipos');
        Schema::dropIfExists('rayosx_objprueba');
        Schema::dropIfExists('rayosx_toe');
        

    }
}
