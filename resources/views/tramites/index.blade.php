@extends('layouts.layout')
@section('barnav')
<nav class="navbar navbar-expand-md mt-2">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
        aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <div class="mr-2">
            <div class="locator mb-2">
                <div class="point"><span></span></div>
            </div>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/registroSolicitud')}}">Ventanilla Ùnica</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{url('/tramiteRayosX')}}">Licencia Rayos X</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="#">Autorización de Títulos</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="#">Soporte y Ayuda</a>
                </li>

                @guest
                    <li class="nav-item"><a class="nav-link" href="#registro">Iniciar o Registrarse</a></li>
                @else
                    <li class="nav-item">
                    <div class="dropdown">
                        <a href="#" class="nav-link">Mi Cuenta</a>
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="{{ route('home') }}">
                              Portal Ventanilla Unica
                          </a>
                          <a class="dropdown-item" href="{{ route('home') }}">
                              Mi Perfil
                          </a>
                          <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                              Cerrar Sesión
                          </a>
                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                             {{ csrf_field() }}
                          </form>
                        </div>
                     </div>
                    </li>
                @endguest

            </ul>
        </div>
        <div class="right-content">
                <form action="{{url('/busqueda')}}" method="post">
                    <input name="term" class="form-control mr-sm-2" type="text" placeholder="Buscar" aria-label="Buscar">
                    <button class="buscar"></button>
                </form>
            <div class="dropdown text-center">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    Accesibilidad
                </button>
                <div class="dropdown-menu " aria-labelledby="dropdownMenuButton">

                    <a class="dropdown-item" href="{{url('/accesibilidad')}}">
                    </a>
                </div>
            </div>
        </div>

    </div>
</nav>
@endsection
@section('content')

            <table id="mytable" class="table table-bordred table-striped">
             <thead>
               <th>Id Solicitud</th>
               <th>Tipo Tramite</th>
               <th>Fecha de Creacion</th>
               <th>Fecha Edicion</th>
               <th>Estado</th>
               <th>Descripción Tarea</th>
               <th>Editar</th>
               <th>Eliminar</th>
             </thead>
             <tbody>
              @if($tramites->count())  
              @foreach($tramites as $tramite)  
              <tr>
                <td>{{$tramite->id}}</td>
                <td>{{$tramite->user}}</td>
                <td>{{$tramite->tipo_tramite}}</td>
                <td>{{$tramite->estado}}</td>
                <td>{{$tramite->created_at}}</td>
                <td>{{$tramite->updated_at}}</td>
                <td><a class="btn btn-primary btn-xs" href="{{action('tramitesController@edit', $tramite->id)}}" ><span class="glyphicon glyphicon-pencil"></span></a></td>
                <td>
                  <form action="{{action('tramitesController@destroy', $tramite->id)}}" method="post">
                   {{csrf_field()}}
                   <input name="_method" type="hidden" value="DELETE">

                   <button class="btn btn-danger btn-xs" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                 </td>
               </tr>
               @endforeach 
               @else
               <tr>
                <td colspan="8">No hay registro !!</td>
              </tr>
              @endif
            </tbody>

          </table>       
      {{ $tramites->links() }}    
</section>

@endsection